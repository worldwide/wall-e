<?php

namespace App\Sums\Snippets;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use DB;

class Snippets
{

    const ALERTS = '
    -- alerts
    alert_count,
    alert_amount,
    ';
    const ALERTS_ARRAY = [
    'alert_count',
    'alert_amount'
    ];
    const ALERTS_SUM = '
    -- alerts sum
    SUM(alert_count) AS alert_count,
    SUM(alert_amount) AS alert_amount,
    ';
    const ALERTS_CB_ARRAY = [
    'alert_count' => 'alert_count_by_cb_date',
    'alert_amount' => 'alert_amount_by_cb_date'
    ];
    const ALERTS_ACQ = '
    -- alerts by acquisition date
    alert_count_by_acq_date,
    alert_amount_by_acq_date,
    ';
    const ALERTS_ACQ_ARRAY = [
    'alert_count' => 'alert_count_by_acq_date',
    'alert_amount' => 'alert_amount_by_acq_date'
    ];
    const ALERTS_ACQ_SUM = '
    -- alerts by acquisition date sum
    SUM(alert_count_by_acq_date) AS alert_count_by_acq_date,
    SUM(alert_amount_by_acq_date) AS alert_amount_by_acq_date,
    ';
// ====================================
    const RMAS = '
    -- rma
    rma_count,
    rma_amount,
    ';
    const RMAS_ARRAY = [
    'rma_count',
    'rma_amount'
    ];
    const RMAS_SUM = '
    -- rma sum
    SUM(rma_count) AS rma_count,
    SUM(rma_amount) AS rma_amount,
    ';
    const RMAS_CB_ARRAY = [
    'rma_count' => 'rma_count_by_cb_date',
    'rma_amount' => 'rma_amount_by_cb_date'
    ];
    const RMAS_ACQ = '
    -- rma by acquisition date
    rma_count_by_acq_date,
    rma_amount_by_acq_date,
    ';
    const RMAS_ACQ_ARRAY = [
    'rma_count' => 'rma_count_by_acq_date',
    'rma_amount' => 'rma_amount_by_acq_date'
    ];
    const RMAS_ACQ_SUM = '
    -- rma by acquisition date sum
    SUM(rma_count_by_acq_date) AS rma_count_by_acq_date,
    SUM(rma_amount_by_acq_date) AS rma_amount_by_acq_date,
    ';
// ====================================
    const DECLINES = '
    -- declines
    decline_count,
    decline_amount,
    ';
    const DECLINES_ARRAY = [
    'decline_count',
    'decline_amount'
    ];
    const DECLINES_SUM = '
    -- declines sum
    SUM(decline_count) AS decline_count,
    SUM(decline_amount) AS decline_amount,
    ';
    const DECLINES_CB_ARRAY = [
    'decline_count' => 'decline_count_by_cb_date',
    'decline_amount' => 'decline_amount_by_cb_date'
    ];
    const DECLINES_ACQ = '
    -- declines by acquisition date
    decline_count_by_acq_date,
    decline_amount_by_acq_date,
    ';
    const DECLINES_ACQ_ARRAY = [
    'decline_count' => 'decline_count_by_acq_date',
    'decline_amount' => 'decline_amount_by_acq_date'
    ];
    const DECLINES_ACQ_SUM = '
    -- declines by acquisition date sum
    SUM(decline_count_by_acq_date) AS decline_count_by_acq_date,
    SUM(decline_amount_by_acq_date) AS decline_amount_by_acq_date,
    ';
// ====================================
    const RETURNS = '
    -- returns
    return_count,
    return_amount,
    ';
    const RETURNS_ARRAY = [
    'return_count',
    'return_amount',
    ];
    const RETURNS_SUM = '
    -- returns sum
    SUM(return_count) AS return_count,
    SUM(return_amount) AS return_amount,
    ';
    const RETURNS_CB_ARRAY = [
        'return_count' => 'return_count_by_cb_date',
        'return_amount' => 'return_amount_by_cb_date'
    ];
    const RETURNS_ACQ = '
    -- returns by acquisition
    return_count_by_acq_date,
    return_amount_by_acq_date,
    ';
    const RETURNS_ACQ_ARRAY = [
    'return_count' => 'return_count_by_acq_date',
    'return_amount' => 'return_amount_by_acq_date'
    ];
    const RETURNS_ACQ_SUM = '
    -- returns by acquisition date sum
    SUM(return_count_by_acq_date) AS return_count_by_acq_date,
    SUM(return_amount_by_acq_date) AS return_amount_by_acq_date,
    ';
// ====================================
    const REFUNDS = '
    -- refunds
    refund_count,
    refund_amount,
    ';
    const REFUNDS_ARRAY = [
    'refund_count',
    'refund_amount'
    ];
    const REFUNDS_SUM = '
    -- refunds sum
    SUM(refund_count) AS refund_count,
    SUM(refund_amount) AS refund_amount,
    ';
    const REFUNDS_CB_ARRAY = [
    'refund_count' => 'refund_count_by_cb_date',
    'refund_amount' => 'refund_amount_by_cb_date'
    ];
    const REFUNDS_ACQ = '
    -- refunds by acquisition date
    refund_count_by_acq_date,
    refund_amount_by_acq_date,
    ';
    const REFUNDS_ACQ_ARRAY = [
    'refund_count' => 'refund_count_by_acq_date',
    'refund_amount' => 'refund_amount_by_acq_date'
    ];
    const REFUNDS_ACQ_SUM = '
    -- refunds by acquisition date sum
    SUM(refund_count_by_acq_date) AS refund_count_by_acq_date,
    SUM(refund_amount_by_acq_date) AS refund_amount_by_acq_date,
    ';
// ====================================
    const VOIDS = '
    -- voids
    void_count,
    void_amount,
    ';
    const VOIDS_ARRAY = [
    'void_count',
    'void_amount'
    ];
    const VOIDS_SUM = '
    -- voids sum
    SUM(void_count) AS void_count,
    SUM(void_amount) AS void_amount,
    ';
    const VOIDS_CB_ARRAY = [
    'void_count' => 'void_count_by_cb_date',
    'void_amount' => 'void_amount_by_cb_date'
    ];
    const VOIDS_ACQ = '
    -- voids by acquisition date
    void_count_by_acq_date,
    void_amount_by_acq_date,
    ';
    const VOIDS_ACQ_ARRAY = [
    'void_count' => 'void_count_by_acq_date',
    'void_amount' => 'void_amount_by_acq_date'
    ];
    const VOIDS_ACQ_SUM = '
    -- voids by acquisition date sum
    SUM(void_count_by_acq_date) AS void_count_by_acq_date,
    SUM(void_amount_by_acq_date) AS void_amount_by_acq_date,
    ';
// ====================================
    const QUICKCANCELS = '
    -- quick cancels
    quickcancel_count,
    quickcancel_amount,
    ';
    const QUICKCANCELS_ARRAY = [
    'quickcancel_count',
    'quickcancel_amount'
    ];
    const QUICKCANCELS_SUM = '
    -- quick cancels sum
    SUM(quickcancel_count) AS quickcancel_count,
    SUM(quickcancel_amount) AS quickcancel_amount,
    ';
    const QUICKCANCELS_CB_ARRAY = [
    'quickcancel_count' => 'quickcancel_count_by_cb_date',
    'quickcancel_amount' => 'quickcancel_amount_by_cb_date'
    ];
    const QUICKCANCELS_ACQ = '
    -- quick cancels by acquisition date
    quickcancel_count_by_acq_date,
    quickcancel_amount_by_acq_date,
    ';
    const QUICKCANCELS_ACQ_ARRAY = [
    'quickcancel_count' => 'quickcancel_count_by_acq_date',
    'quickcancel_amount' => 'quickcancel_amount_by_acq_date'
    ];
    const QUICKCANCELS_ACQ_SUM = '
    -- quick cancels by acquisition date sum
    SUM(quickcancel_count_by_acq_date) AS quickcancel_count_by_acq_date,
    SUM(quickcancel_amount_by_acq_date) AS quickcancel_amount_by_acq_date,
    ';
// ====================================
    const BILLINGS = '
    -- billing cycles
    init_count,
    init_amount,
    init_gs_count,
    init_gs_amount,
    rebill_count,
    rebill_amount,
    rebill_gs_count,
    rebill_gs_amount,
    complete_count,
    complete_amount,
    ';
    const BILLINGS_ARRAY = [
    'init_count',
    'init_amount',
    'init_gs_count',
    'init_gs_amount',
    'rebill_count',
    'rebill_amount',
    'rebill_gs_count',
    'rebill_gs_amount',
    'complete_count',
    'complete_amount'
    ];
    const BILLINGS_SUM = '
    -- billing cycles sum
    SUM(init_count) AS init_count,
    SUM(init_amount) AS init_amount,
    SUM(init_gs_count) AS init_gs_count,
    SUM(init_gs_amount) AS init_gs_amount,
    SUM(rebill_count) AS rebill_count,
    SUM(rebill_amount) AS rebill_amount,
    SUM(rebill_gs_count) AS rebill_gs_count,
    SUM(rebill_gs_amount) AS rebill_gs_amount,
    SUM(complete_count) AS complete_count,
    SUM(complete_amount) AS complete_amount,
    ';
    const BILLINGS_CB_ARRAY = [
    'init_count' => 'init_count_by_cb_date',
    'init_amount' => 'init_amount_by_cb_date',
    'init_gs_count' => 'init_gs_count_by_cb_date',
    'init_gs_amount' => 'init_gs_amount_by_cb_date',
    'rebill_count' => 'rebill_count_by_cb_date',
    'rebill_amount' => 'rebill_amount_by_cb_date',
    'rebill_gs_count' => 'rebill_gs_count_by_cb_date',
    'rebill_gs_amount' => 'rebill_gs_amount_by_cb_date',
    'complete_count' => 'complete_count_by_cb_date',
    'complete_amount' => 'complete_amount_by_cb_date'
    ];
    const BILLINGS_ACQ = '
    -- billing cycles by acquisition date
    init_count_by_acq_date,
    init_amount_by_acq_date,
    init_gs_count_by_acq_date,
    init_gs_amount_by_acq_date,
    rebill_count_by_acq_date,
    rebill_amount_by_acq_date,
    rebill_gs_count_by_acq_date,
    rebill_gs_amount_by_acq_date,
    complete_count_by_acq_date,
    complete_amount_by_acq_date,
    ';
    const BILLINGS_ACQ_ARRAY = [
    'init_count'=>'init_count_by_acq_date',
    'init_amount' => 'init_amount_by_acq_date',
    'init_gs_count'=>'init_gs_count_by_acq_date',
    'init_gs_amount'=>'init_gs_amount_by_acq_date',
    'rebill_count'=>'rebill_count_by_acq_date',
    'rebill_amount'=>'rebill_amount_by_acq_date',
    'rebill_gs_count'=>'rebill_gs_count_by_acq_date',
    'rebill_gs_amount'=>'rebill_gs_amount_by_acq_date',
    'complete_count'=>'complete_count_by_acq_date',
    'complete_amount'=>'complete_amount_by_acq_date'
    ];
    const BILLINGS_ACQ_SUM = '
    -- billing cycles by acquisition date sum
    SUM(init_count_by_acq_date) AS init_count_by_acq_date,
    SUM(init_amount_by_acq_date) AS init_amount_by_acq_date,
    SUM(init_gs_count_by_acq_date) AS init_gs_count_by_acq_date,
    SUM(init_gs_amount_by_acq_date) AS init_gs_amount_by_acq_date,
    SUM(rebill_count_by_acq_date) AS rebill_count_by_acq_date,
    SUM(rebill_amount_by_acq_date) AS rebill_amount_by_acq_date,
    SUM(rebill_gs_count_by_acq_date) AS rebill_gs_count_by_acq_date,
    SUM(rebill_gs_amount_by_acq_date) AS rebill_gs_amount_by_acq_date,
    SUM(complete_count_by_acq_date) AS complete_count_by_acq_date,
    SUM(complete_amount_by_acq_date) AS complete_amount_by_acq_date,
    ';
// ====================================
    const CHARGEBACKS = '
    -- charge backs
    chargeback_count,
    chargeback_amount,
    ';
    const CHARGEBACKS_ARRAY = [
    'chargeback_count',
    'chargeback_amount'
    ];
    const CHARGEBACKS_SUM = '
    -- charge backs sum
    SUM(chargeback_count) AS chargeback_count,
    SUM(chargeback_amount) AS chargeback_amount,
    ';
    const CHARGEBACKS_ACQ = '
    -- charge backs by acquisition date
    chargeback_count_by_acq_date,
    chargeback_amount_by_acq_date,
    ';
    const CHARGEBACKS_TRANS = '
    -- charge backs by transaction date
    chargeback_count_by_trans_date,
    chargeback_amount_by_trans_date,
    ';
    const CHARGEBACKS_TRANS_ARRAY = [
    'chargeback_count'=>'chargeback_count_by_trans_date',
    'chargeback_amount'=>'chargeback_amount_by_trans_date'
    ];
    const CHARGEBACKS_TRANS_SUM = '
    -- charge backs by transaction date sum
    SUM(chargeback_count_by_trans_date) AS chargeback_count_by_trans_date,
    SUM(chargeback_amount_by_trans_date) AS chargeback_amount_by_trans_date,
    ';
    const CHARGEBACKS_ACQ_ARRAY = [
    'chargeback_count' => 'chargeback_count_by_acq_date',
    'chargeback_amount' => 'chargeback_amount_by_acq_date'
    ];
    const CHARGEBACKS_ACQ_SUM = '
    -- charge backs by transaction date sum
    SUM(chargeback_count_by_acq_date) AS chargeback_count_by_acq_date,
    SUM(chargeback_amount_by_acq_date) AS chargeback_amount_by_acq_date,
    ';
// ====================================
    const GROSSNET = '
    -- gross/net
    gross_count,
    gross_amount,
    net_amount
    ';
    const GROSSNET_SUM = '
    -- gross/net sum
    SUM(gross_count) AS gross_count,
    SUM(gross_amount) AS gross_amount,
    SUM(net_amount) AS net_amount
    ';
    const GROSSNET_CB_ARRAY = [
    'gross_amount' => 'gross_amount_by_cb_date',
    'net_amount' => 'net_amount_by_cb_date'
    ];
    const GROSSNET_ACQ = '
    -- gross/net by acquisition date
    gross_amount_by_acq_date,
    net_amount_by_acq_date
    ';
    const GROSSNET_ACQ_ARRAY = [
    'gross_amount'=>'gross_amount_by_acq_date',
    'net_amount'=>'net_amount_by_acq_date'
    ];
    const GROSSNET_ACQ_SUM = '
    -- gross/net by acquisition date sum
    SUM(gross_amount_by_acq_date) AS gross_amount_by_acq_date,
    SUM(net_amount_by_acq_date) AS net_amount_by_acq_date
    ';
// ====================================
    const GENERAL =
    self::ALERTS . "\n" .
    self::REFUNDS . "\n" .
    self::VOIDS . "\n" .
    self::QUICKCANCELS . "\n" .
    self::RETURNS . "\n" .
    self::RMAS . "\n" .
    self::BILLINGS . "\n" .
    self::CHARGEBACKS . "\n" .
    self::DECLINES . "\n" .
    self::GROSSNET . "\n"
    ;
    const GENERAL_SUM =
    self::ALERTS_SUM . "\n" .
    self::REFUNDS_SUM . "\n" .
    self::VOIDS_SUM . "\n" .
    self::QUICKCANCELS_SUM . "\n" .
    self::RETURNS_SUM . "\n" .
    self::RMAS_SUM . "\n" .
    self::BILLINGS_SUM . "\n" .
    self::CHARGEBACKS_SUM . "\n" .
    self::DECLINES_SUM . "\n" .
    self::GROSSNET_SUM
    ;
    const GENERAL_ACQ =
    self::ALERTS_ACQ . "\n" .
    self::REFUNDS_ACQ . "\n" .
    self::VOIDS_ACQ . "\n" .
    self::QUICKCANCELS_ACQ . "\n" .
    self::RETURNS_ACQ . "\n" .
    self::RMAS_ACQ . "\n" .
    self::BILLINGS_ACQ . "\n" .
    self::CHARGEBACKS_ACQ . "\n" .
    self::DECLINES_ACQ . "\n" .
    self::GROSSNET_ACQ . ",\n"
    ;
    const GENERAL_TRANS =
    self::CHARGEBACKS_TRANS
    ;
    const GENERAL_ACQ_SUM =
    self::ALERTS_ACQ_SUM . "\n" .
    self::REFUNDS_ACQ_SUM . "\n" .
    self::VOIDS_ACQ_SUM . "\n" .
    self::QUICKCANCELS_ACQ_SUM . "\n" .
    self::RETURNS_ACQ_SUM . "\n" .
    self::RMAS_ACQ_SUM . "\n" .
    self::BILLINGS_ACQ_SUM . "\n" .
    self::CHARGEBACKS_ACQ_SUM . "\n" .
    self::DECLINES_ACQ_SUM . "\n" .
    self::GROSSNET_ACQ_SUM . ",\n"
    ;
    const GENERAL_TRANS_SUM =
    self::CHARGEBACKS_TRANS_SUM
    ;
// ====================================


    const STAMP_DAY = '
    -- stamp day
    pivot_date,
    ';
    const STAMP_DAY_SUM = '
    -- stamp day sum
    pivot_date,
    ';

    const STAMP_WEEK = '
    -- stamp week
    pivot_year,
    pivot_week,
    ';
    const STAMP_WEEK_SUM = '
    -- stamp week sum
    YEAR(pivot_date) AS pivot_year,
    WEEK(pivot_date) AS pivot_week,
    ';

    const STAMP_MONTH = '
    -- stamp month
    pivot_year,
    pivot_month,
    ';
    const STAMP_MONTH_SUM = '
    -- stamp month sum
    YEAR(pivot_date) AS pivot_year,
    MONTH(pivot_date) AS pivot_month,
    ';

    const STAMP_YEAR = '
    -- stamp year
    pivot_year,
    ';
    const STAMP_YEAR_SUM = '
    -- stamp year sum
    pivot_year,
    ';

    const STEP = '
    product_step,
    ';


    public static function dbInsert($query, $fields = null, $myClass, $myFunc, $info = null){

        try {
            $startStamp = time();
            $rowCount = DB::connection('dst_db')->insert($query, $fields);
            self::log($startStamp, $myClass, $myFunc, count($rowCount), $info);
        } catch (\PDOException $e) {
//            Bugsnag::notifyException($e);
            throw $e;
        }

    }

    public static function dbSelect($query, $fields, $myClass, $myFunc, $info = null){

        try {
            $startStamp = time();
            $rowCount = DB::connection('src_db')->select($query, $fields);
            self::log($startStamp, $myClass, $myFunc, count($rowCount), $info);
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        return $rowCount;
    }
    public static function updateAcqOffsets($toTable, $ids){

        $fields = array_merge(
            self::ALERTS_ACQ_ARRAY,
            self::REFUNDS_ACQ_ARRAY,
            self::VOIDS_ACQ_ARRAY,
            self::QUICKCANCELS_ACQ_ARRAY,
            self::RETURNS_ACQ_ARRAY,
            self::RMAS_ACQ_ARRAY,
            self::BILLINGS_ACQ_ARRAY,
            self::CHARGEBACKS_ACQ_ARRAY,
            self::DECLINES_ACQ_ARRAY
        );

        foreach($ids AS $id){
            $whereLines[] = "t1.$id = t2.$id";
            $idFields[] = $id;
        }
        foreach($fields AS $key=>$val){
            $setLines[] = "t1.$val = t2.$val";
            $sumLines[] = "SUM($key) AS $val";
        }
        $selectFields = implode(" , \n", $idFields).','.implode(" , \n", $sumLines);
        $groupByFields = implode(" , \n", $idFields);
        $setFields = implode(" , \n", $setLines);
        $whereFields = implode(" AND\n ", $whereLines);

        $query = "
        UPDATE $toTable AS t1,
        (
        SELECT
            $selectFields,
            acquisition_date AS acq_date

            FROM orders
            GROUP BY 
              $groupByFields,
              acquisition_date 
        ) AS t2
        SET
            $setFields
        WHERE
            $whereFields AND 
            t1.pivot_date = t2.acq_date
        ";

        self::dbInsert($query,[],__CLASS__, __FUNCTION__, "fromTable: orders toTable: $toTable");
    }

    public static function updateCbOffsets($toTable, $ids){

        $fields = array_merge(
            self::ALERTS_CB_ARRAY,
            self::REFUNDS_CB_ARRAY,
            self::VOIDS_CB_ARRAY,
            self::QUICKCANCELS_CB_ARRAY,
            self::RETURNS_CB_ARRAY,
            self::RMAS_CB_ARRAY,
            self::BILLINGS_CB_ARRAY,
            self::DECLINES_CB_ARRAY
        );

        foreach($ids AS $id){
            $whereLines[] = "t1.$id = t2.$id";
            $idFields[] = $id;
        }
        foreach($fields AS $key=>$val){
            $setLines[] = "t1.$val = t2.$val";
            $sumLines[] = "SUM($key) AS $val";
        }
        $selectFields = implode(" , \n", $idFields).','.implode(" , \n", $sumLines);
        $groupByFields = implode(" , \n", $idFields);
        $setFields = implode(" , \n", $setLines);
        $whereFields = implode(" AND\n ", $whereLines);

        $query = "
        UPDATE $toTable AS t1,
        (
        SELECT
            $selectFields,
            chargeback_date AS cb_date

            FROM orders
            GROUP BY 
              $groupByFields,
              chargeback_date 
        ) AS t2
        SET
            $setFields
        WHERE
            $whereFields AND 
            t1.pivot_date = t2.cb_date
        ";

        self::dbInsert($query,[],__CLASS__, __FUNCTION__, "fromTable: orders toTable: $toTable");
    }

    public static function updateBillTransOffsets($fromTable, $toTable, $ids){

        $fields = array_merge(
            self::ALERTS_BILLTRANS_ARRAY,
            self::REFUNDS_BILLTRANS_ARRAY,
            self::VOIDS_BILLTRANS_ARRAY,
            self::QUICKCANCELS_BILLTRANS_ARRAY,
            self::RETURNS_BILLTRANS_ARRAY,
            self::RMAS_BILLTRANSARRAY,
            self::BILLINGS_BILLTRANS_ARRAY,
            self::DECLINES_BILLTRANS_ARRAY
        );

        foreach($ids AS $id){
            $whereLines[] = "t1.$id = t2.$id";
            $idFields[] = $id;
        }
        foreach($fields AS $key=>$val){
            $setLines[] = "t1.$val = t2.$val";
            $sumLines[] = "SUM($key) AS $val";
        }
        $selectFields = implode(" , \n", $idFields).','.implode(" , \n", $sumLines);
        $groupByFields = implode(" , \n", $idFields);
        $setFields = implode(" , \n", $setLines);
        $whereFields = implode(" AND\n ", $whereLines);

        $query = "
        UPDATE $toTable AS t1,
        (
        SELECT
            $selectFields,
            chargeback_date AS cb_date

            FROM $fromTable
            WHERE $whereFields
            GROUP BY 
              $groupByFields,
              chargeback_date 
        ) AS t2
        SET
            $setFields,
            t1.chargeback_date = t2.cb_date
        WHERE
            $whereFields AND 
            t1.chargeback_date = t2.cb_date
        ";

        self::dbInsert($query,[],__CLASS__, __FUNCTION__, "fromTable: $fromTable toTable: $toTable");
    }

    public static function log($start, $myClass, $myFunc, $rowCount, $actionInfo = ''){
        $duration = time() - $start;
        $myClass = (strstr($myClass,'\\')) ? substr(strrchr($myClass, '\\'), 1) : $myClass;
        echo "$myClass->$myFunc($actionInfo) \t\t$rowCount row(s) affected ($duration sec)\n";
    }

    public static function glue($keys, $rec)
    {
        $myAssoc = [];
        foreach($keys AS $key){
            $myAssoc[$key] = $rec->$key;
        }
        return $myAssoc;
    }

    public static function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {

            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }





}
