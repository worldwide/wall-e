<?php

namespace App\Contracts;

interface Sumtract
{
    function day($date);

    function week($week, $year);

    function month($month, $year);

    function year($year);
}
