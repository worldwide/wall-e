<?php

namespace App\Collections;

use App\Sums\Snippets\Snippets;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use DB;

class Campaigns
{

    const IDS = 'campaign_id';
    const IDS_ARRAY = ['campaign_id'];

    public static function daySum(){

        try {
            DB::connection('dst_db')->insert('DELETE FROM campaigns_day_sum');
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        $query = "INSERT INTO campaigns_day_sum
        (
            ".self::IDS.",

            ".Snippets::STAMP_DAY."

            ".Snippets::GENERAL."
        )
        SELECT
            ".self::IDS.",

            ".Snippets::STAMP_DAY_SUM."

            ".Snippets::GENERAL_SUM."

        FROM orders
        WHERE 
        -- only 1 billing type
        (
        IF(init_count>0,1,0) +
        IF(init_gs_count>0,1,0) +
        IF(rebill_count>0,1,0) +
        IF(rebill_gs_count>0,1,0) +
        IF(complete_count>0,1,0)
        ) = 1
        GROUP BY pivot_date, ".self::IDS."
        
        ";

        Snippets::dbInsert($query,[],__CLASS__, __FUNCTION__, 'daySum');
        Snippets::updateAcqOffsets('campaigns_day_sum',self::IDS_ARRAY);
        Snippets::updateCbOffsets('campaigns_day_sum',self::IDS_ARRAY);

    }

    public static function weekSum(){

        try {
            DB::connection('dst_db')->insert('DELETE FROM campaigns_week_sum');
        } catch (\PDOException $e) {
//            Bugsnag::notifyException($e);
            throw $e;
        }

        $query = "INSERT INTO campaigns_week_sum
        (
            ".self::IDS.",

            ".Snippets::STAMP_WEEK."

            ".Snippets::GENERAL_ACQ."
            ".Snippets::GENERAL_TRANS."
            ".Snippets::GENERAL."
        )
            
            SELECT 
                
            ".self::IDS.",

            ".Snippets::STAMP_WEEK_SUM."

            ".Snippets::GENERAL_ACQ_SUM."
            ".Snippets::GENERAL_TRANS_SUM."
            ".Snippets::GENERAL_SUM."
            
            FROM campaigns_day_sum
            
            GROUP BY pivot_year, pivot_week, ".self::IDS;

        Snippets::dbInsert($query,[],__CLASS__, __FUNCTION__, 'weekSum');
    }

    public static function monthSum(){

        try {
            DB::connection('dst_db')->insert('DELETE FROM campaigns_month_sum');
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        $query = "INSERT INTO campaigns_month_sum
        (
            ".self::IDS.",
            
            ".Snippets::STAMP_MONTH."

            ".Snippets::GENERAL_ACQ."
            ".Snippets::GENERAL_TRANS."
            ".Snippets::GENERAL."
        )
            
            SELECT 
                
            ".self::IDS.",

            ".Snippets::STAMP_MONTH_SUM."

            ".Snippets::GENERAL_ACQ_SUM."
            ".Snippets::GENERAL_TRANS_SUM."
            ".Snippets::GENERAL_SUM."
                                    
            FROM campaigns_day_sum
            
            GROUP BY pivot_year, pivot_month, ".self::IDS;

        Snippets::dbInsert($query,[],__CLASS__, __FUNCTION__, 'monthSum');

    }

    public static function yearSum(){

        try {
            DB::connection('dst_db')->insert('DELETE FROM campaigns_year_sum');
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        $query = "INSERT INTO campaigns_year_sum
        (

            ".self::IDS.",

            ".Snippets::STAMP_YEAR."

            ".Snippets::GENERAL_ACQ."
            ".Snippets::GENERAL_TRANS."
            ".Snippets::GENERAL."

        )
            SELECT 

            ".self::IDS.",

            ".Snippets::STAMP_YEAR_SUM."

            ".Snippets::GENERAL_ACQ_SUM."
            ".Snippets::GENERAL_TRANS_SUM."
            ".Snippets::GENERAL_SUM."
            
            FROM campaigns_month_sum
            
            GROUP BY pivot_year, ".self::IDS;

        Snippets::dbInsert($query,[],__CLASS__, __FUNCTION__,'year: all');
    }

    public static function baseTableSum(){

        try {
            DB::connection('dst_db')->insert('DELETE FROM campaigns');
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        $query = "INSERT INTO campaigns
        (
            ".self::IDS.",            

            ".Snippets::GENERAL."
        )
        SELECT
            ".self::IDS.",

            ".Snippets::GENERAL_SUM."
            
        FROM campaigns_year_sum
        GROUP BY ".self::IDS."
        ";

        Snippets::dbInsert($query,[],__CLASS__, __FUNCTION__,'all');

    }

}
