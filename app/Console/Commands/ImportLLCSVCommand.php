<?php

namespace App\Console\Commands;

use App\Sums\Snippets\Snippets;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Sources\LLCSV AS LLCSV;
use TijsVerkoyen\CssToInlineStyles\Exception;

class ImportLLCSVCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import:LLCSV";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'imports data from external sources';

    const ROWUPSERT = "REPLACE LOW_PRIORITY INTO orders (

        order_id, 
        order_ancestor_id,
        order_parent_id,
        campaign_id, 
        customer_id,
        gateway_id,
        product_id, 

        affiliate_id, 
        subaffiliate_id,

        order_status,

        sale_date, 
        acquisition_date,
        chargeback_date,

        chargeback_amount,
        order_total,
        decline_amount,
        rma_amount,
        refund_amount,
        return_amount,
        void_amount

      ) VALUES (

        :order_id, 
        :order_ancestor_id,
        :order_parent_id,
        :campaign_id, 
        :customer_id, 
        :gateway_id, 
        :product_id, 

        :affiliate_id, 
        :subaffiliate_id,

        :order_status,

        :sale_date, 
        :acquisition_date,
        :chargeback_date,

        :chargeback_amount,
        :order_total,
        :decline_amount,
        :rma_amount,
        :refund_amount,
        :return_amount,
        :void_amount
        
    )
    ";

    private $checkSet = [
        'rules'=> [
            'order_id'=>'required|filled|numeric|min:4',
            'order_ancestor_id'=>'required|numeric',
            'order_parent_id'=>'required|numeric',
            'campaign_id'=>'required|numeric',
            'customer_id'=>'required|numeric',
            'gateway_id'=>'required|numeric',
            'product_id'=>'required|numeric',

            'affiliate_id'=>'numeric',
            'subaffiliate_id'=>'present|numeric',

            'order_status'=>'required|numeric',

            'sale_date'=>'required|date_format:Y-m-d',
            'acquisition_date'=>'required|date_format:Y-m-d',
            'chargeback_date'=>'date_format:Y-m-d',

            'order_total'=>'required|numeric',
            'chargeback_amount'=>'required_with:chargeback_date|numeric',
            'decline_amount'=>'required_with:order_total|numeric',
            'rma_amount'=>'required_with:order_total|numeric',
            'refund_amount'=>'required_with:order_total|numeric',
            'return_amount'=>'required_with:order_total|numeric',
            'void_amount'=>'required_with:order_total|numeric'
        ],
        'msgs' => [
            'required'=>'the :attribute field is required'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "populating LLCSV...";

        $source = new LLCSV();
        $counter = 0;
        while ( ($rec = $source->step()) !== FALSE ){
            $validator = Validator::make($rec, $this->checkSet['rules'],$this->checkSet['msgs']);
            // is invalid record
            if($validator->fails()){
                print_r($rec);
                print_r($validator->errors()->all());
                $e = new Exception('validation failed!');
                Bugsnag::notifyException($e);
                throw $e;
            } else {

                try {
                    DB::connection('dst_db')->insert(self::ROWUPSERT, $rec);
                    $counter++;
                } catch (\PDOException $e) {
                    $e = new Exception("can't insert db record");
                    Bugsnag::notifyException($e);
                    throw $e;
                }

            }
        }

        echo "DONE $counter line(s)\n";
    }
}
