<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ShowSchemaCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate mysql DB schema text';
    protected $signature = 'show:schema';
    private $tables = [
        'gateways' => [
            'columns' => [
                'merchant_id' => 'MEDIUMINT(8) UNSIGNED NOT NULL',
                'is_domestic' => 'TINYINT(1) UNSIGNED DEFAULT NULL',
                'is_trial' => 'TINYINT(1) UNSIGNED DEFAULT NULL',
                'entity' => 'VARCHAR(200) NOT NULL',
                'msp' => 'VARCHAR(200)',
                'platform' => 'VARCHAR(200)',
                'bank' => 'VARCHAR(200)',
                'active_date' => 'DATE',
                'closed_date' => 'DATE',
                'step' => "SET('1','2')",
                'mid_alias' => 'VARCHAR(200) NOT NULL',
                'descriptor' => 'VARCHAR(200) NOT NULL',
                'product_type' => "SET('skin','nutra','mifinity')",
                'terminal_id' => 'TINYINT(3)',
                'mcc_code' => 'VARCHAR(10)'
            ]
        ]
    ];
    private $tableSets = [
        'affiliates' => [
            'keys' => ['affiliate_id']
        ],
        'affiliates_steps' => [
            'keys' => ['affiliate_id','step']
        ],
        'campaigns' => [
            'keys' => ['campaign_id']
        ],
        'campaigns_products' => [
            'keys' => ['campaign_id','product_id']
        ],
        'campaigns_steps' => [
            'keys' => ['campaign_id','step']
        ],
        'campaigns_subaffiliates' => [
            'keys' => ['campaign_id','affiliate_id','subaffiliate_id']
        ],
        'customers' => [
            'keys' => ['customer_id']
        ],
        'customers_steps' => [
            'keys' => ['customer_id','step']
        ],
        'gateways' => [
            'keys' => ['gateway_id']
        ],
        'gateways_products' => [
            'keys' => ['gateway_id','product_id']
        ],
        'gateways_steps' => [
            'keys' => ['gateway_id','step']
        ],
        'gateways_subaffiliates' => [
            'keys' => ['gateway_id','affiliate_id','subaffiliate_id']
        ],
        'products' => [
            'keys' => ['product_id']
        ],
        'steps' => [
            'keys' => ['step']
        ],
        'subaffiliates' => [
            'keys' => ['affiliate_id','subaffiliate_id']
        ],
        'subaffiliates_steps' => [
            'keys' => ['affiliate_id','subaffiliate_id','step']
        ]
    ];

    private $columns = [
        # ID fields
        'customer_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'affiliate_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'subaffiliate_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'product_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'order_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'campaign_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'gateway_id' => [
            'cfg'=>'MEDIUMINT(8) NOT NULL'
        ],
        'step' => [
            'cfg'=>'TINYINT(3) UNSIGNED NOT NULL'
        ],

        'card_type' => [
            'cfg'=>"ENUM('visa','master','discover')"
        ],
        'card_type_count' => [
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],

        # sum fields
        'net_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'net_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'gross_count' => [
            'tags'=>'all',
            'cfg'=>'TINYINT(3) UNSIGNED NOT NULL'
        ],
        'gross_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'gross_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # pivot fields
        'pivot_date' => [
            'tags'=>['day'],
            'cfg'=>'DATE NOT NULL'
        ],
        'pivot_week' => [
            'tags'=>['week','year'],
            'cfg'=>'TINYINT(3) UNSIGNED NOT NULL'
        ],
        'pivot_month' => [
            'tags'=>['month','year'],
            'cfg'=>'TINYINT(3) UNSIGNED NOT NULL'
        ],
        'pivot_year' => [
            'tags'=>['week','year'],
            'cfg'=>'SMALLINT(3) UNSIGNED NOT NULL'
        ],

        # alerts
        'alert_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'alert_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'alert_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'alert_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'alert_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'alert_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # declines
        'decline_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'decline_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'decline_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'decline_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'decline_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'decline_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # chargebacks
        'chargeback_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'chargeback_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'chargeback_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'chargeback_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'chargeback_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'chargeback_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'chargeback_count_by_trans_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'chargeback_amount_by_trans_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # refunds
        'refund_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'refund_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'refund_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'refund_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'refund_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'refund_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # returns
        'return_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'return_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'return_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'return_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'return_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'return_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # rmas
        'rma_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rma_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rma_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rma_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rma_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rma_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # quickcancels
        'quickcancel_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'quickcancel_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'quickcancel_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'quickcancel_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'quickcancel_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'quickcancel_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # voids
        'void_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'void_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'void_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'void_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'void_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'void_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],


        # init (billing cycle)
        'init_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'init_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'init_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # init_gs (billing cycle)
        'init_gs_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_gs_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'init_gs_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_gs_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'init_gs_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'init_gs_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # rebill (billing cycle)
        'rebill_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rebill_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rebill_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # rebill_gs (billing cycle)
        'rebill_gs_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_gs_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rebill_gs_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_gs_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'rebill_gs_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'rebill_gs_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],

        # complete (billing cycle)
        'complete_count' => [
            'tags'=>'all',
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'complete_amount' => [
            'tags'=>'all',
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'complete_count_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'complete_amount_by_acq_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ],
        'complete_count_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0'
        ],
        'complete_amount_by_cb_date' => [
            'tags'=>['day','week','month','year'],
            'cfg'=>'DOUBLE(10,2) UNSIGNED NOT NULL DEFAULT 0.00'
        ]


    ];


    public function __construct()
    {
        parent::__construct();

    }

    /**
     * updates gateways lookup table with current data
     *
     * @return mixed
     */
    public function handle()
    {


        echo $this->fetchSql();

        return true;

    }

    private function fetchSql()
    {
        $output = '';
        foreach($this->tableSets AS $setName=>$val){
            $output .= $this->fetchTableSetSql($setName, $val['keys']) . "\n";
        }

        return $output;
    }

    private function fetchTableSetSql($name, $keys)
    {

        return  $this->fetchTableSql($name, $keys, 'base')."\n\n".
                $this->fetchTableSql($name, $keys, 'day')."\n\n".
                $this->fetchTableSql($name, $keys, 'week')."\n\n".
                $this->fetchTableSql($name, $keys, 'month')."\n\n".
                $this->fetchTableSql($name, $keys, 'year');
    }

    private function fetchTableSql($tableName, $keyIds, $type)
    {

        switch($type){
            case 'base':
                $tail = '';
                break;
            case 'day':
                $tail = '_day_sum';
                $keyIds = array_merge($keyIds, ['pivot_date']);
                break;
            case 'week':
                $tail = '_week_sum';
                $keyIds = array_merge($keyIds, ['pivot_week','pivot_year']);
                break;
            case 'month':
                $tail = '_month_sum';
                $keyIds = array_merge($keyIds, ['pivot_month','pivot_year']);
                break;
            case 'year':
                $tail = '_year_sum';
                $keyIds = array_merge($keyIds, ['pivot_year']);
                break;
        }

        // fetch default columns (if present)
        if( isset($this->tables[$tableName]['columns']) ){
            foreach($this->tables[$tableName]['columns'] AS $colName => $colCfg) {
                $customColumns[] = $colName . ' ' . $colCfg;
            }
        } else {
            $customColumns = [];
        }

        // fetch default columns
        $defaultColumns = $this->fetchColumnsSql($keyIds, $type);

        // add primary key columns to the array
        $primaryKey = 'PRIMARY KEY ('.implode(',',$keyIds).')';

        // implode everything and return it
        $columnsSql = implode(",\n",array_merge($defaultColumns, $customColumns, [$primaryKey]));

        return "
        DROP TABLE IF EXISTS $tableName{$tail};
        CREATE TABLE $tableName{$tail} (
        $columnsSql
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

    }

    private function fetchColumnsSql($keys, $type)
    {
        $items = [];

        foreach($this->columns AS $colName => $colCfg){

            // add column if it exists in the tables keys array
            if( in_array($colName, $keys) ) {
                $items[] = $colName .' '. $colCfg['cfg'];
                continue;
            }

            // add column if it is tagged for all table types
            if( isset($colCfg['tags']) && is_string($colCfg['tags']) && $colCfg['tags'] === 'all' ) {
                $items[] = $colName .' '. $colCfg['cfg'];
                continue;
            }

            // add column if the table type is in the tags array
            if( isset($colCfg['tags']) && is_array($colCfg['tags']) && in_array($type, $colCfg['tags'])) {
                $items[] = $colName .' '. $colCfg['cfg'];
                continue;
            }

        }

        return $items;
    }

}
