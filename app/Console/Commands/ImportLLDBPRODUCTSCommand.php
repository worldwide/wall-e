<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;
use DB;
use TijsVerkoyen\CssToInlineStyles\Exception;
use App\Sources\LLDBPRODUCTS AS LLDBPRODUCTS;

class ImportLLDBPRODUCTSCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import:LLDBPRODUCTS";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import fields dependant on LL db products table';

    const ROWUPSERT = "UPDATE LOW_PRIORITY orders
        SET 
        step = :step,
        init_count = :init_count,
        init_gs_count = :init_gs_count,
        rebill_count = :rebill_count,
        rebill_gs_count = :rebill_gs_count,
        complete_count = :complete_count
    WHERE product_id = :product_id
    ";

    private $checkSet = [
        'rules'=> [
            'product_id'=>'required|numeric',
            'step'=>'required|numeric',
            'init_count'=>'required|in:0,1',
            'init_gs_count'=>'required|in:0,1',
            'rebill_count'=>'required|in:0,1',
            'rebill_gs_count'=>'required|in:0,1',
            'complete_count'=>'required|in:0,1'
        ],
        'msgs' => [
            'required'=>'the :attribute field is required'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "populating LLDBPRODUCTS...\n";
        $counter=0;

        $source = new LLDBPRODUCTS();
        $results = $source->run();
        foreach($results AS $row) {
            $row = (array)$row;
            $validator = Validator::make($row, $this->checkSet['rules'],$this->checkSet['msgs']);
            // is invalid record
            if($validator->fails()){

                print_r($row);
                print_r($validator->errors()->all());
                $e = new Exception('validation failed!');
                Bugsnag::notifyException($e);

                throw $e;
            } else {

                echo "updating product_id: {$row['product_id']} step: {$row['step']} init_count: {$row['init_count']} init_gs_count: {$row['init_gs_count']} rebill_count: {$row['rebill_count']} rebill_gs_count: {$row['rebill_gs_count']} complete_count: {$row['complete_count']}\n";
                try {
                    DB::connection('dst_db')->insert(self::ROWUPSERT, $row);
                } catch (\PDOException $e) {
                    Bugsnag::notifyException($e);
                    throw $e;
                }

            }
            try {
                DB::connection('dst_db')->insert(self::ROWUPSERT, (array)$row);
            } catch (\PDOException $e) {
                Bugsnag::notifyException($e);
                throw $e;
            }
            $counter++;
        }

        echo "DONE $counter product(s)\n";
    }
}