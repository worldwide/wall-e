<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use DB;

class ImportCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'imports data from external sources';

    public function __construct()
    {
        parent::__construct();

    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // import data
        Artisan::call('import:LLCSV');
        Artisan::call('import:LLDBNOTES');
        Artisan::call('import:LLDBPRODUCTS');

        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        // populate summary tables
        Artisan::call('sum');
        // append data to summary data
        Artisan::call('import:LLDBGATEWAYS');

    }
}
