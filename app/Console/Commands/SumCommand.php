<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Console\Command;
use App\Collections;
use DB;

class SumCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "sum";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'updates sum tables';

    public function __construct()
    {
        parent::__construct();

    }

    const ROWUPDATE = "UPDATE LOW_PRIORITY orders 
    SET
      alert_amount = IF(alert_count=1 AND order_total>0,order_total,alert_amount),
      quickcancel_amount = IF(quickcancel_count=1 AND order_total>0,order_total,quickcancel_amount),
      chargeback_count = IF(chargeback_amount > 0,1,chargeback_count),
      void_count = IF(void_amount > 0,1,void_count),
      decline_count = IF(decline_amount > 0,1,decline_count),

      rma_count = IF(rma_amount > 0,1,0),

      return_count = IF(return_amount > 0,1,0),

      refund_count = IF(refund_amount > 0,1,0),

      init_amount = IF(init_count=1,order_total,init_amount),
      init_gs_amount = IF(init_gs_count=1,order_total,init_gs_amount),
      rebill_amount = IF(rebill_count=1,order_total,rebill_amount),
      rebill_gs_amount = IF(rebill_gs_count=1,order_total,rebill_gs_amount),
      complete_amount = IF(complete_count=1,order_total,complete_amount)

    ";

    const GROSSNETUPDATE = "UPDATE LOW_PRIORITY orders 
    SET
      gross_amount = order_total,
      gross_count = 1,
      net_amount = order_total - (void_amount + decline_amount + rma_amount + refund_amount)
    ";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // patch data after everything has been imported
            echo "patch data after everything has been imported\n";
            DB::connection('dst_db')->update(self::ROWUPDATE);
            DB::connection('dst_db')->update(self::GROSSNETUPDATE);
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }

        Collections\Affiliates::daySum();
        Collections\Affiliates::weekSum();
        Collections\Affiliates::monthSum();
        Collections\Affiliates::yearSum();
        Collections\Affiliates::baseTableSum();

        Collections\AffiliatesSteps::daySum();
        Collections\AffiliatesSteps::weekSum();
        Collections\AffiliatesSteps::monthSum();
        Collections\AffiliatesSteps::yearSum();
        Collections\AffiliatesSteps::baseTableSum();

        Collections\Campaigns::daySum();
        Collections\Campaigns::weekSum();
        Collections\Campaigns::monthSum();
        Collections\Campaigns::yearSum();
        Collections\Campaigns::baseTableSum();

        Collections\CampaignsSteps::daySum();
        Collections\CampaignsSteps::weekSum();
        Collections\CampaignsSteps::monthSum();
        Collections\CampaignsSteps::yearSum();
        Collections\CampaignsSteps::baseTableSum();

        Collections\CampaignsSubaffiliates::daySum();
        Collections\CampaignsSubaffiliates::weekSum();
        Collections\CampaignsSubaffiliates::monthSum();
        Collections\CampaignsSubaffiliates::yearSum();
        Collections\CampaignsSubaffiliates::baseTableSum();

        Collections\Customers::daySum();
        Collections\Customers::weekSum();
        Collections\Customers::monthSum();
        Collections\Customers::yearSum();
        Collections\Customers::baseTableSum();

        Collections\CustomersSteps::daySum();
        Collections\CustomersSteps::weekSum();
        Collections\CustomersSteps::monthSum();
        Collections\CustomersSteps::yearSum();
        Collections\CustomersSteps::baseTableSum();

        Collections\Gateways::daySum();
        Collections\Gateways::weekSum();
        Collections\Gateways::monthSum();
        Collections\Gateways::yearSum();
        Collections\Gateways::baseTableSum();

        Collections\GatewaysProducts::daySum();
        Collections\GatewaysProducts::weekSum();
        Collections\GatewaysProducts::monthSum();
        Collections\GatewaysProducts::yearSum();
        Collections\GatewaysProducts::baseTableSum();

        Collections\GatewaysSteps::daySum();
        Collections\GatewaysSteps::weekSum();
        Collections\GatewaysSteps::monthSum();
        Collections\GatewaysSteps::yearSum();
        Collections\GatewaysSteps::baseTableSum();

        Collections\GatewaysSubaffiliates::daySum();
        Collections\GatewaysSubaffiliates::weekSum();
        Collections\GatewaysSubaffiliates::monthSum();
        Collections\GatewaysSubaffiliates::yearSum();
        Collections\GatewaysSubaffiliates::baseTableSum();

        Collections\Products::daySum();
        Collections\Products::weekSum();
        Collections\Products::monthSum();
        Collections\Products::yearSum();
        Collections\Products::baseTableSum();

        Collections\Subaffiliates::daySum();
        Collections\Subaffiliates::weekSum();
        Collections\Subaffiliates::monthSum();
        Collections\Subaffiliates::yearSum();
        Collections\Subaffiliates::baseTableSum();

    }
}
