<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;
use DB;
use TijsVerkoyen\CssToInlineStyles\Exception;
use App\Sources\LLDBNOTES AS LLDBNOTES;

class ImportLLDBNOTESCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import:LLDBNOTES";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import fields dependant on LL db notes table';

    const ROWUPDATE = "UPDATE LOW_PRIORITY orders 
    SET 
      alert_count = :alert_count,
      quickcancel_count = :quickcancel_count
    WHERE order_id = :order_id
    ";

    private $checkSet = [
        'rules'=> [
            'order_id'=>'required|filled|numeric',
            'alert_count'=>'required|in:0,1',
            'quickcancel_count'=>'required|in:0,1'
        ],
        'msgs' => [
            'required'=>'the :attribute field is required'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "populating LLDBNOTES...\n";
        $counter=0;

        $source = new LLDBNOTES();
        $results = $source->run();
        try {
            echo "resetting alert_count and quickcancel_count columns in orders table\n";
            DB::connection('dst_db')->update('UPDATE orders SET alert_count=0, quickcancel_count=0');
        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }
        foreach($results AS $row) {
            $row = (array)$row;
            $validator = Validator::make($row, $this->checkSet['rules'],$this->checkSet['msgs']);
            // is invalid record
            if($validator->fails()){

                print_r($row);
                print_r($validator->errors()->all());
                $e = new Exception('validation failed!');
                Bugsnag::notifyException($e);
                throw $e;
            } else {

                try {
                    DB::connection('dst_db')->update(self::ROWUPDATE, $row);
                    echo "updating order_id: {$row['order_id']} alert_count: {$row['alert_count']} quickcancel_count: {$row['quickcancel_count']}\n";
                } catch (\PDOException $e) {
                    Bugsnag::notifyException($e);
                    throw $e;
                }

            }
            $counter++;
        }

        echo "DONE $counter notes(s)\n";
    }
}