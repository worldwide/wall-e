<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;
use DB;
use TijsVerkoyen\CssToInlineStyles\Exception;
use App\Sources\LLDBORDERS AS LLDBORDERS;

class ImportLLDBORDERSCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import:LLDBORDERS";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import fields dependant on LL db orders table';

    const ROWUPDATE = "UPDATE orders
        SET 
        card_type = :card_type
    WHERE order_id = :order_id
    ORDER BY order_id
    ";

    private $checkSet = [
        'rules'=> [
            'order_id'=>'required|numeric',
            'card_type'=>'required|in:visa,discover,master'
        ],
        'msgs' => [
            'required'=>'the :attribute field is required'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "populating LLDBORDERS...\n";
        $counter=0;

        $source = new LLDBORDERS();
        $results = $source->run();
        foreach($results AS $row) {
            $row = (array)$row;
            $validator = Validator::make($row, $this->checkSet['rules'],$this->checkSet['msgs']);
            // is invalid record
            if($validator->fails()){

                print_r($row);
                print_r($validator->errors()->all());
                $e = new Exception('validation failed!');
                Bugsnag::notifyException($e);

                throw $e;
            } else {

//                echo "updating order_id: {$row['order_id']} card_type: {$row['card_type']}\n";
                try {
                    DB::connection('dst_db')->insert(self::ROWUPDATE, $row);
                } catch (\PDOException $e) {

                    Bugsnag::notifyException($e);
                    throw $e;
                }

            }
            $counter++;
        }

        echo "DONE $counter order(s)\n";
    }
}