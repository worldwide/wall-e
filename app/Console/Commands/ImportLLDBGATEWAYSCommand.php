<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;
use DB;
use TijsVerkoyen\CssToInlineStyles\Exception;
use App\Sources\LLDBGATEWAYS AS LLDBGATEWAYS;

class ImportLLDBGATEWAYSCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = "import:LLDBGATEWAYS";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import fields dependant on LL db gateways table';

    const ROWUPDATE = "UPDATE gateways
        SET 
        merchant_id = :merchant_id,
        is_domestic = :is_domestic,
        is_trial = :is_trial,
        entity = :entity,
        msp = :msp,
        platform = :platform,
        bank = :bank,
        active_date = :active_date,
        closed_date = :closed_date,
        step = :step,
        mid_alias = :mid_alias,
        descriptor = :descriptor,
        product_type = :product_type,
        terminal_id = :terminal_id,
        mcc_code = :mcc_code
    WHERE gateway_id = :gateway_id
    ";

    private $checkSet = [
        'rules'=> [
            'gateway_id'=>'required|numeric',
            'merchant_id'=>'string',
            'is_domestic'=>'in:0,1',
            'is_trial'=>'in:0,1',
            'entity'=>'string',
            'msp'=>'string',
            'platform'=>'string',
            'bank'=>'string',
            'active_date'=>'present',
            'closed_date'=>'present',
            'step'=>'present',
            'mid_alias'=>'present',
            'descriptor'=>'required',
            'product_type'=>'present',
            'terminal_id'=>'present',
            'mcc_code'=>'present'
        ],
        'msgs' => [
            'required'=>'the :attribute field is required'
        ]
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "populating LLDBGATEWAYS...\n";
        $counter=0;

        $source = new LLDBGATEWAYS();

        $results = $source->run();

        foreach($results AS $row) {
            $row = (array)$row;
            $validator = Validator::make($row, $this->checkSet['rules'],$this->checkSet['msgs']);
            // is invalid record
            if($validator->fails()){

                print_r($row);
                print_r($validator->errors()->all());
                $e = new Exception('validation failed!');
                Bugsnag::notifyException($e);

                throw $e;
            } else {

                echo "updating...\n";
                print_r($row);
                try {
                    DB::connection('dst_db')->insert(self::ROWUPDATE, $row);
                } catch (\PDOException $e) {

                    Bugsnag::notifyException($e);
                    throw $e;
                }

            }
            $counter++;
        }

        echo "DONE $counter product(s)\n";
    }
}