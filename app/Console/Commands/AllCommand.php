<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Command;

class AllCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update everything';
    protected $signature = 'all
        {--date= : the date to process, defaults to today}
        {--to= : the optional date to process to}
    ';

    public function __construct()
    {
        parent::__construct();

    }

    /**
     *
     * updates all lookup tables with current data
     *
     * @return mixed

     */
    public function handle()
    {

        Artisan::call('updatetransactions', [
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updateaffiliates',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updatesubaffiliates',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updateproducts',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updategateways',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
//        Artisan::call('updategatewayssubaffiliates',[
//            '--date' => $this->option('date'),
//            '--to' => $this->option('to')
//        ]);
//        Artisan::call('updateorders',[
//            '--date' => $this->option('date'),
//            '--to' => $this->option('to')
//        ]);
        Artisan::call('updatecustomers',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updatecampaigns',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
        Artisan::call('updateacquisitions',[
            '--date' => $this->option('date'),
            '--to' => $this->option('to')
        ]);
//        Artisan::call('updatecampaignsproducts',[
//            '--date' => $this->option('date'),
//            '--to' => $this->option('to')
//        ]);

    }
}
