<?php

namespace App\Console;

//use Bugsnag\BugsnagLaravel\Commands\DeployCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // to refresh all lookup tables
        'App\Console\Commands\ImportLLCSVCommand',
        'App\Console\Commands\ImportLLDBGATEWAYSCommand',
        'App\Console\Commands\ImportLLDBORDERSCommand',
        'App\Console\Commands\ImportLLDBNOTESCommand',
        'App\Console\Commands\ImportLLDBPRODUCTSCommand',
        // to update by date only and auto updated all dependant sum tables
        'App\Console\Commands\ShowSchemaCommand',
        'App\Console\Commands\SumCommand',
        'App\Console\Commands\ImportCommand'

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
