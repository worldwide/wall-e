<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class OrdersProductsTransactions
{

    const IDS = '
    -- ID field(s)
    order_id  		    MEDIUMINT UNSIGNED NOT NULL,
    product_id  		MEDIUMINT UNSIGNED NOT NULL,  
    transaction_id	    MEDIUMINT UNSIGNED NOT NULL,
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS orders_products_transactions_day_sum;
        CREATE TABLE orders_products_transactions_day_sum (
            '.self::IDS.'  

            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(order_id, product_id, transaction_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS orders_products_transactions_week_sum;
        CREATE TABLE orders_products_transactions_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(order_id, product_id, transaction_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS orders_products_transactions_month_sum;
        CREATE TABLE orders_products_transactions_month_sum (
            '.self::IDS.'  

            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(order_id, product_id, transaction_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS orders_products_transactions_year_sum;
        CREATE TABLE orders_products_transactions_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
       
            PRIMARY KEY(order_id, product_id, transaction_id, created_year)
        );        
    ';

    const ALL =
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";

}