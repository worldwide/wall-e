<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class Orders
{

    const IDS = '
    -- ID field(s)
    order_id  		MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS orders;
        CREATE TABLE orders (
            '.self::IDS.'  

            parent_id 			MEDIUMINT UNSIGNED NOT NULL,
            ancestor_id 		MEDIUMINT UNSIGNED NOT NULL,
            customer_id 		MEDIUMINT UNSIGNED NOT NULL,
            campaign_id 		MEDIUMINT UNSIGNED NOT NULL,
            affiliate_id 		MEDIUMINT UNSIGNED NOT NULL,
            subaffiliate_id 	MEDIUMINT UNSIGNED NOT NULL,

            is_quick_cancel		BOOL NOT NULL DEFAULT 0 COMMENT "customer cancels within 48 hours",

            '.BLK::STAMP_DAY.'

            '.BLK::RMAS.'
            '.BLK::RETURNS.'
            '.BLK::REFUNDS.'
            '.BLK::VOIDS.'
            '.BLK::ALERTS.'
            '.BLK::BILLINGS.'
            '.BLK::CHARGEBACKS.'
            '.BLK::HOLDS.'

            '.BLK::PROFITS.'
	                
            PRIMARY KEY(order_id)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS orders_day_sum;
        CREATE TABLE orders_day_sum (
                        
            '.BLK::STAMP_DAY.'

            crm_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold due to failed charge attempts",
            emp_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold at customers request",
            quick_cancel_count	MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# customers cancels within 48 hours",
	
            '.BLK::RMAS.'
            '.BLK::RETURNS.'
            '.BLK::REFUNDS.'
            '.BLK::VOIDS.'
            '.BLK::ALERTS.'
            '.BLK::BILLINGS.'
            '.BLK::CHARGEBACKS.'

            '.BLK::PROFITS.'

            PRIMARY KEY(created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS orders_week_sum;
        CREATE TABLE orders_week_sum (
                
            '.BLK::STAMP_WEEK.'

            crm_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold due to failed charge attempts",
            emp_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold at customers request",
            quick_cancel_count	MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# customers cancels within 48 hours",

            '.BLK::RMAS.'
            '.BLK::RETURNS.'
            '.BLK::REFUNDS.'
            '.BLK::VOIDS.'
            '.BLK::ALERTS.'
            '.BLK::BILLINGS.'
            '.BLK::CHARGEBACKS.'

            '.BLK::PROFITS.'
	
            PRIMARY KEY(created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS orders_month_sum;
        CREATE TABLE orders_month_sum (
        
            '.BLK::STAMP_MONTH.'

            crm_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold due to failed charge attempts",
            emp_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold at customers request",
            quick_cancel_count	MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# customers cancels within 48 hours",

            '.BLK::RMAS.'
            '.BLK::RETURNS.'
            '.BLK::REFUNDS.'
            '.BLK::VOIDS.'
            '.BLK::ALERTS.'
            '.BLK::BILLINGS.'
            '.BLK::CHARGEBACKS.'

            '.BLK::PROFITS.'
	        
	        
            PRIMARY KEY(created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS orders_year_sum;
        CREATE TABLE orders_year_sum (
        
            '.BLK::STAMP_YEAR.'

            crm_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold due to failed charge attempts",
            emp_hold_count		MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# orders on hold at customers request",
            quick_cancel_count	MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "# customers cancels within 48 hours",

            '.BLK::RMAS.'
            '.BLK::RETURNS.'
            '.BLK::REFUNDS.'
            '.BLK::VOIDS.'
            '.BLK::ALERTS.'
            '.BLK::BILLINGS.'
            '.BLK::CHARGEBACKS.'

            '.BLK::PROFITS.'
	
	
            PRIMARY KEY(created_year)
        );        
    ';

    const ALL =
    self::MAIN."\n".
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";

}