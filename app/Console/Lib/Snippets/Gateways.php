<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class Gateways
{
    const IDS = '
    -- ID field(s)
    gateway_id  		MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS gateways;
        CREATE TABLE gateways (
            '.self::IDS.'  
                        
            label 				VARCHAR(100) NOT NULL,
        
            PRIMARY KEY(gateway_id)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS gateways_day_sum;
        CREATE TABLE gateways_day_sum (
            '.self::IDS.'  
            
            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'


            PRIMARY KEY(gateway_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS gateways_week_sum;
        CREATE TABLE gateways_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(gateway_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS gateways_month_sum;
        CREATE TABLE gateways_month_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(gateway_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS gateways_year_sum;
        CREATE TABLE gateways_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(gateway_id, created_year)
        );        
    ';

    const ALL =
    self::MAIN
    ."\n".self::DAY
    ."\n".self::WEEK
    ."\n".self::MONTH
    ."\n".self::YEAR;

}