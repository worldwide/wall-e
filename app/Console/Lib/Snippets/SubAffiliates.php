<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class SubAffiliates
{
    const IDS = '
    -- ID field(s)
    subaffiliate_id			MEDIUMINT UNSIGNED NOT NULL,
    affiliate_id			MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS subaffiliates;
        CREATE TABLE subaffiliates (
            '.self::IDS.'  
                
            '.BLK::MISC.'
                
            PRIMARY KEY(affiliate_id, subaffiliate_id)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS subaffiliates_day_sum;
        CREATE TABLE subaffiliates_day_sum (
            '.self::IDS.'  
            
            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'


            PRIMARY KEY(subaffiliate_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS subaffiliates_week_sum;
        CREATE TABLE subaffiliates_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(subaffiliate_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS subaffiliates_month_sum;
        CREATE TABLE subaffiliates_month_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(subaffiliate_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS subaffiliates_year_sum;
        CREATE TABLE subaffiliates_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle_num	MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(subaffiliate_id, created_year)
        );        
    ';

    const ALL =
    self::MAIN."\n".
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";


}