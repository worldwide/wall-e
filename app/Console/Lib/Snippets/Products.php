<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class Products
{

    const IDS = '
    -- ID field(s)
    product_id  		MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS products;
        CREATE TABLE products (
            '.self::IDS.'  
        
            label 				VARCHAR(200) NOT NULL,
        	bill_cycle			SET(\'VAP\',\'Unknown\',\'Test\',\'SSS\',\'SS\',\'Reship\',\'Reorder\',\'Rebill\',\'Init\',\'Comp\',\'Closed\',\'AddOn\') NOT NULL,        

            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
                
            PRIMARY KEY(product_id)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS products_day_sum;
        CREATE TABLE products_day_sum (
            '.self::IDS.'  
                        
            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(product_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS products_week_sum;
        CREATE TABLE products_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(product_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS products_month_sum;
        CREATE TABLE products_month_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(product_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS products_year_sum;
        CREATE TABLE products_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(product_id, created_year)
        );        
    ';

    const ALL =
    self::MAIN."\n".
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";

}