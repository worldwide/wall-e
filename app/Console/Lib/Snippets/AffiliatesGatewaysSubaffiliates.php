<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class AffiliatesGatewaysSubaffiliates
{

    const IDS = '
	-- ID field(s)
	affiliate_id		MEDIUMINT UNSIGNED NOT NULL,
	gateway_id			MEDIUMINT UNSIGNED NOT NULL,
	subaffiliate_id		MEDIUMINT UNSIGNED NOT NULL,
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS affiliates_gateways_subaffiliates_day_sum;
        CREATE TABLE affiliates_gateways_subaffiliates_day_sum (
            '.self::IDS.'  

            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(affiliate_id, gateway_id, subaffiliate_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS affiliates_gateways_subaffiliates_week_sum;
        CREATE TABLE affiliates_gateways_subaffiliates_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            PRIMARY KEY(affiliate_id, gateway_id, subaffiliate_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS affiliates_gateways_subaffiliates_month_sum;
        CREATE TABLE affiliates_gateways_subaffiliates_month_sum (
            '.self::IDS.'  

            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(affiliate_id, gateway_id, subaffiliate_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS affiliates_gateways_subaffiliates_year_sum;
        CREATE TABLE affiliates_gateways_subaffiliates_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            PRIMARY KEY(affiliate_id, gateway_id, subaffiliate_id, created_year)
        );        
    ';

    const ALL =
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";

}