<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class CampaignsProducts
{

    const IDS = '
    -- ID field(s)
    campaign_id  		MEDIUMINT UNSIGNED NOT NULL,
    product_id  		MEDIUMINT UNSIGNED NOT NULL,  
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS campaigns_products_day_sum;
        CREATE TABLE campaigns_products_day_sum (
            '.self::IDS.'  

            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle			VARCHAR(100) NOT NULL,
            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            bill_cycle_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "total billing cycles",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",

            PRIMARY KEY(campaign_id, product_id, bill_cycle, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS campaigns_products_week_sum;
        CREATE TABLE campaigns_products_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle			VARCHAR(100) NOT NULL,
            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            bill_cycle_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "total billing cycles",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(campaign_id, product_id, bill_cycle, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS campaigns_products_month_sum;
        CREATE TABLE campaigns_products_month_sum (
            '.self::IDS.'  

            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            bill_cycle			VARCHAR(100) NOT NULL,
            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            bill_cycle_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "total billing cycles",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",

            PRIMARY KEY(campaign_id, product_id, bill_cycle, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS campaigns_products_year_sum;
        CREATE TABLE campaigns_products_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle			VARCHAR(100) NOT NULL,
            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            bill_cycle_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "total billing cycles",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(campaign_id, product_id, bill_cycle, created_year)
        );        
    ';

    const ALL =
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";

}