<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class Campaigns
{

    const IDS = '
    -- ID field(s)
    campaign_id  		MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS campaigns;
        CREATE TABLE campaigns (
            '.self::IDS.'  
        
            label 				VARCHAR(200) NOT NULL,
            ship_insur_type		SET(\'GS\',\'PC\') COMMENT "guarantee ship / pay certify",
        
            '.BLK::MISC.'
                
            PRIMARY KEY(campaign_id)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS campaigns_day_sum;
        CREATE TABLE campaigns_day_sum (
            '.self::IDS.'  
                        
            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'


            PRIMARY KEY(campaign_id, created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS campaigns_week_sum;
        CREATE TABLE campaigns_week_sum (
            '.self::IDS.'  
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(campaign_id, created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS campaigns_month_sum;
        CREATE TABLE campaigns_month_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            bill_cycle_num	MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",

            PRIMARY KEY(campaign_id, created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS campaigns_year_sum;
        CREATE TABLE campaigns_year_sum (
            '.self::IDS.'  
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle_num	MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(campaign_id, created_year)
        );        
    ';

    const ALL =
    self::MAIN."\n".
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";


}