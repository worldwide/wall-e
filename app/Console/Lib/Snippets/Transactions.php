<?php

namespace App\Console\Lib\Snippets;

use App\Console\Lib\Snippets\SqlColumnBlocks AS BLK;

class Transactions
{

    const IDS = '
    -- ID field(s)
    transaction_id  	MEDIUMINT UNSIGNED NOT NULL,
    created_dt	 		DATETIME NOT NULL,
	order_id 	 		MEDIUMINT UNSIGNED NOT NULL,
	gateway_id  		MEDIUMINT UNSIGNED NOT NULL,
	customer_id  		MEDIUMINT UNSIGNED NOT NULL,
	product_id  		MEDIUMINT UNSIGNED NOT NULL,
	campaign_id  		MEDIUMINT UNSIGNED NOT NULL,
	affiliate_id  		MEDIUMINT UNSIGNED NOT NULL,
	subaffiliate_id  	MEDIUMINT UNSIGNED NOT NULL,
    ';

    const MAIN = '
        DROP TABLE IF EXISTS transactions;
        CREATE TABLE transactions (
            '.self::IDS.'  
        
            '.BLK::MISC.'

            bill_cycle			SET(\'VAP\',\'Unknown\',\'Test\',\'SSS\',\'SS\',\'Reship\',\'Reorder\',\'Rebill\',\'Init\',\'Comp\',\'Closed\',\'AddOn\') NOT NULL,
        	bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL,

        	alert_type			SET(\'EMS\',\'ETHOCA\') COMMENT "alerts are manually counted and optional",
            amount				DOUBLE(10,2) NOT NULL DEFAULT 0.00,
            is_chargeback 		BOOL NOT NULL,
            chargeback_date		DATE,

            ship_insur_type		SET(\'GS\',\'PC\') COMMENT "guarantee ship / pay certify",
                        
            PRIMARY KEY(transaction_id),
            INDEX (is_chargeback)
        )
        PARTITION BY LIST(MONTH(created_dt)) (
            PARTITION pJan VALUES IN (1),
            PARTITION pFeb VALUES IN (2),
            PARTITION pMar VALUES IN (3),
            PARTITION pApr VALUES IN (4),
            PARTITION pMay VALUES IN (5),
            PARTITION pJun VALUES IN (6),
            PARTITION pJul VALUES IN (7),
            PARTITION pAug VALUES IN (8),
            PARTITION pSep VALUES IN (9),
            PARTITION pOct VALUES IN (10),
            PARTITION pNov VALUES IN (11),
            PARTITION pDec VALUES IN (12)
        );
    ';

    const DAY = '
        
        DROP TABLE IF EXISTS transactions_day_sum;
        CREATE TABLE transactions_day_sum (
                        
            '.BLK::STAMP_DAY.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'


            PRIMARY KEY(created_dt)
        );
    ';

    const WEEK = '        
        DROP TABLE IF EXISTS transactions_week_sum;
        CREATE TABLE transactions_week_sum (
                
            '.BLK::STAMP_WEEK.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle_num		MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(created_week, created_year)
        );
    ';

    const MONTH = '
        DROP TABLE IF EXISTS transactions_month_sum;
        CREATE TABLE transactions_month_sum (
        
            '.BLK::STAMP_MONTH.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'
        
            bill_cycle_num	MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",

            PRIMARY KEY(created_month, created_year)
        ); 
    ';

    const YEAR = '
        DROP TABLE IF EXISTS transactions_year_sum;
        CREATE TABLE transactions_year_sum (
        
            '.BLK::STAMP_YEAR.'
            '.BLK::MISC.'
            '.BLK::TRANS_AND_CB.'

            bill_cycle_num	MEDIUMINT UNSIGNED NOT NULL COMMENT "billing cycle #",
            ship_insur_count	MEDIUMINT UNSIGNED NOT NULL COMMENT "guarantee ship / pay certify",
        
            PRIMARY KEY(created_year)
        );        
    ';

    const ALL =
    'DROP DATABASE IF EXISTS reports;
    CREATE DATABASE reports;
    
    USE reports;'."\n".
    self::MAIN."\n".
    self::DAY."\n".
    self::WEEK."\n".
    self::MONTH."\n".
    self::YEAR."\n";


}