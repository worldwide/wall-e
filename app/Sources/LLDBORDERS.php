<?php

namespace App\Sources;
ini_set("memory_limit",-1);

use DB;

class LLDBORDERS
{

    const QUERY = "SELECT 
        order_id,
        cc_type AS card_type
    FROM analytics.limelight_orders
    WHERE order_id >= 9009730
    ";

    public function run(){
        try {
            $srcResults = DB::connection('src_db')->select(self::QUERY);
        } catch (\PDOException $e) {
            die($e);
        }
        
        return $srcResults;

    }




}
