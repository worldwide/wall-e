<?php

namespace App\Sources;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use DB;

class LLDBGATEWAYS
{

    const QUERY = "SELECT 

        gatewayId AS gateway_id,
        merchId AS merchant_id,
        isDomestic AS is_domestic,
        isTrial AS is_trial,
        entity,
        msp,
        processingPlatform AS platform,
        acquiringBank AS bank,
        dateActive AS active_date,
        dateClosed AS closed_date,
        isStep1 AS step,
        midAlias AS mid_alias,
        descriptor,
        isNutra AS product_type,
        tid AS terminal_id,
        mcc_code AS mcc

    FROM analytics.gateway";

    public function run(){
        try {
            $srcResults = DB::connection('src_db')->select(self::QUERY);


            $dstResults = [];
            foreach($srcResults AS $oldRow){

                $oldRow=(array)$oldRow;
                $row = [];


                $row['gateway_id']=$oldRow['gateway_id'];

                // only alphanum
                $row['merchant_id']=preg_replace("/^\\*/", "", $oldRow['merchant_id']);

                // 0,1 or null
                switch($oldRow['is_domestic']){
                    case null:
                        $row['is_domestic'] = null;
                        break;
                    case 1:
                        $row['is_domestic'] = 1;
                        break;
                    case 2:
                        $row['is_domestic'] = 2;
                        break;
                    default:
                        Bugsnag::notifyException(new \Exception('unknown is_domestic value: ' . $oldRow['is_domestic']));
                }

                // 0,1 or null
                switch($oldRow['is_trial']){
                    case null:
                        $row['is_trial'] = null;
                        break;
                    case 1:
                        $row['is_trial'] = 1;
                        break;
                    case 2:
                        $row['is_trial'] = 2;
                        break;
                    default:
                        Bugsnag::notifyException(new \Exception('unknown is_trial value: ' . $oldRow['is_trial']));
                }


                if(empty($oldRow['entity'])){
                    $row['entity']=null;
                } else {
                    $row['entity']=$oldRow['entity'];
                }

                if(empty($oldRow['msp'])){
                    $row['msp']=null;
                } else {
                    $row['msp']=$oldRow['msp'];
                }

                if(empty($oldRow['platform'])){
                    $row['platform']=null;
                } else {
                    $row['platform']=$oldRow['platform'];
                }

                if(empty($oldRow['bank'])){
                    $row['bank']=null;
                } else {
                    $row['bank']=$oldRow['bank'];
                }

                $row['active_date']=$oldRow['active_date'];
                $row['closed_date']=$oldRow['closed_date'];

                // SET(1,2) can be null
                switch($oldRow['step']){
                    case null:
                        $row['step'] = null;
                        break;
                    case 1:
                        $row['step'] = 1;
                        break;
                    case 2:
                        $row['step'] = 2;
                        break;
                    case 3:
                        $row['step'] = '1,2';
                        break;
                    default:
                        Bugsnag::notifyException(new \Exception('unknown Step value: ' . $oldRow['step']));
                }


                // strip out *
                $row['mid_alias']=preg_replace("/^\\*/", "", $oldRow['mid_alias']);
                $row['descriptor']=$oldRow['descriptor'];

                // SET skin,nutra,unknown
                switch($oldRow['product_type']){
                    case null:
                        $row['product_type'] = null;
                        break;
                    case 0:
                        $row['product_type'] = 'skin';
                        break;
                    case 1:
                        $row['product_type'] = 'nutra';
                        break;
                    case 2:
                        $row['product_type'] = 'mifinity';
                        break;
                    case 3:
                        $row['product_type'] = 'skin,nutra';
                        break;
                    default:
                        Bugsnag::notifyException(new \Exception('unknown product_type value: ' . $oldRow['product_type']));

                }

                if(empty($oldRow['terminal_id'])){
                    $row['terminal_id']=null;
                } else {
                    $row['terminal_id']=$oldRow['terminal_id'];
                }

                if(empty($oldRow['mcc_code'])){
                    $row['mcc_code']=null;
                } else {
                    $row['mcc_code']=$oldRow['mcc_code'];
                }

                $dstResults[] = $row;

            }

        } catch (\PDOException $e) {
            Bugsnag::notifyException($e);
            throw $e;
        }
        
        return $dstResults;

    }




}
