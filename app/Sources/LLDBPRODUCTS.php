<?php

namespace App\Sources;

use DB;

class LLDBPRODUCTS
{

    const QUERY = "SELECT 
        step,
        product_id,
        CASE 
            WHEN subscription_cycle = 'Init' AND is_GS = 0 THEN 1
            ELSE 0
        END AS init_count,
        CASE 
            WHEN subscription_cycle = 'Init' AND is_GS = 1 AND is_placeholder = 0 THEN 1
            ELSE 0
        END AS init_gs_count,
        CASE 
            WHEN subscription_cycle = 'Rebill' AND is_GS = 0 THEN 1
            ELSE 0
        END AS rebill_count,
        CASE 
            WHEN subscription_cycle = 'Rebill' AND is_GS = 1 AND is_placeholder = 0 THEN 1
            ELSE 0
        END AS rebill_gs_count,
        CASE 
            WHEN subscription_cycle = 'Comp' THEN 1
            ELSE 0
        END AS complete_count

    FROM analytics.products
        HAVING 1 IN (init_count,init_gs_count,rebill_count,rebill_gs_count,complete_count)
    ";

    public function run(){
        try {
            $srcResults = DB::connection('src_db')->select(self::QUERY);
        } catch (\PDOException $e) {
            die($e);
        }

        return $srcResults;

    }


}
