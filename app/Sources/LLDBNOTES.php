<?php

namespace App\Sources;

use App\Sums\Snippets\Snippets;
use DB;

class LLDBNOTES
{

    const QUERY = "SELECT 
        order_id,
        CASE 
            WHEN message LIKE '%ETHOCA%' OR message LIKE '%ECS%' THEN 1
            ELSE 0
        END AS alert_count,
        CASE 
            WHEN message LIKE 
                '%48CN%' THEN 1
            ELSE 0
        END AS quickcancel_count

    FROM analytics.limelight_order_notes
    HAVING alert_count=1 OR quickcancel_count=1
    ";

    public function run(){
        try {
            $srcResults = DB::connection('src_db')->select(self::QUERY);
        } catch (\PDOException $e) {
            die($e);
        }
        
        return $srcResults;

    }




}
