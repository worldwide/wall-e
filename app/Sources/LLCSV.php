<?php

namespace App\Sources;

use App\Sums\Snippets\Snippets;

use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;

//use DB;


class LLCSV
{

    private $handle;
    private $isHeader = true;
    private $header = null;

    public function __construct()
    {
        $fileName = '/export.csv';
        $this->handle = fopen($fileName,'r');

    }

    private function patchRow($row){

        $row['acquisition_date/time']=date('Y-m-d',strtotime($row['acquisition_date/time']));
        $row['date_of_sale']=date('Y-m-d',strtotime($row['date_of_sale']));
        $row['order_total'] = round($row['order_total'],2);

        // fix chargeback date
        if(!empty($row['chargeback_date'])){
            $row['chargeback_date']=date('Y-m-d',strtotime($row['chargeback_date']));
        }

        // fix affiliate_id count/amount
        if(!empty($row['afid']) && is_numeric($row['afid']) ) {
            $row['affiliate_id'] = $row['afid'];
        } else if(!empty($row['afid']) && !is_numeric($row['afid']) ){

            throw new \Exception("bad afid: {$row['afid']} order_id: {$row['order_id']}");

        }

        // fix subaffiliate_id count/amount
        if(!empty($row['sid']) && is_numeric($row['sid']) ) {
            $row['subaffiliate_id'] = $row['sid'];
        } else if(!empty($row['sid']) && !is_numeric($row['sid']) ){

            throw new \Exception("bad sid: {$row['sid']} order_id: {$row['order_id']}");

        }

        // fix rma count/amount
        if(in_array($row['is_rma'],['YES','Yes','yes','1',1])) {
            $row['rma_amount'] = round($row['order_total'], 2);
        } else if(in_array($row['is_rma'],['NO','No','no','0',0])){
            $row['rma_amount'] = 0;
        } else {
            throw new \Exception("bad is_rma: {$row['is_rma']} order_total: {$row['order_total']} order_id: {$row['order_id']}");
        }

        // fix return count/amount
        if(in_array($row['return'],['YES','Yes','yes','1',1])) {
            $row['return_amount'] = round($row['order_total'], 2);
        } else if(in_array($row['return'],['NO','No','no','0',0])){
            $row['return_amount'] = 0;
        } else {
            throw new \Exception("bad return: {$row['return']} order_id: {$row['order_id']}");
        }

        // fix chargeback count/amount
        if(in_array($row['is_chargeback'],['YES','Yes','yes','1',1])) {
            $row['chargeback_amount'] = round($row['order_total'], 2);
        } else if(in_array($row['is_chargeback'],['NO','No','no','0',0])){
            $row['chargeback_amount'] = 0;
        } else {
            throw new \Exception("bad is_chargeback: {$row['is_chargeback']} order_id: {$row['order_id']}");
        }

        // fix order_status
        switch(strtolower($row['order_status'])){
            case 'new':
                $row['order_status'] = 2;
                $row['decline_amount'] = 0;
                break;
            case 'void/refunded':
            case 'void/refund':
                $row['order_status'] = 6;
//                $row['void_count'] = 1;
                $row['decline_amount'] = 0;

                break;
            case 'declined':
                $row['order_status'] = 7;
                $row['decline_amount'] = round($row['order_total'],2);
                break;
            case 'shipped':
                $row['order_status'] = 8;
                $row['decline_count'] = 0;
                $row['decline_amount'] = 0;
                break;
            default:
                throw new \Exception("bad order_status: {$row['order_status']} order_id: {$row['order_id']}");
                break;
        }

        // fix refund count/amount
        if(in_array($row['is_refund'],['YES','Yes','yes','1',1])) {
            $row['refund_amount'] = round($row['order_total'], 2);
        } else if(in_array($row['is_refund'],['NO','No','no','0',0])) {
            $row['refund_amount'] = 0;
        } else {
            throw new \Exception("bad is_refund: {$row['is_refund']} order_id: {$row['order_id']}");
        }

        // fix void count/amount
        if(!empty($row['void_amount']) || in_array($row['is_void'],['YES','Yes','yes','1']) || $row['order_status']==6){
            $row['void_amount'] = round($row['void_amount'] | $row['order_total'],2);
        } else if(!empty($row['void_amount']) || in_array($row['is_void'],['NO','No','no','0',0])){
            $row['void_amount'] = 0;
        } else {
            throw new \Exception("bad void_amount: {$row['void_amount']} order_id: {$row['order_id']}");
        }

        return $row;
    }

    private function remapKeys($row){

        // new keys => old keys
        $keys = [

            'order_id'=>'order_id',
            'order_ancestor_id'=>'ancestor_order_id',
            'order_parent_id'=>'parent_order_id',
            'campaign_id'=>'campaign_id',
            'affiliate_id'=>'afid',
            'subaffiliate_id'=>'sid',
            'gateway_id'=>'gateway_id',
            'customer_id'=>'customer_number',
            'product_id'=>'product_id',

            'acquisition_date'=>'acquisition_date/time',
            'chargeback_date'=>'chargeback_date',
            'sale_date'=>'date_of_sale',

            'order_total'=>'order_total',
            'rma_amount'=>'rma_amount',
            'refund_amount'=>'refund_amount',
            'return_amount'=>'return_amount',
            'void_amount'=>'void_amount',
            'chargeback_amount'=>'chargeback_amount',
            'quickcancel_amount'=>'quickcancel_amount',
            'decline_amount'=>'decline_amount',

            'order_status'=>'order_status'
        ];

        $newRow = [];
        foreach($keys AS $newKey=>$oldKey){
            if(!isset($row[$oldKey])) continue;
            $newRow[$newKey] = $row[$oldKey];
        }

        return $newRow;
    }

    public function step(){


        $cleanKeys = function ($key){
            return str_replace(' ', '_', strtolower($key));
        };

        $row = fgetcsv($this->handle);
        if($row === FALSE){
            return FALSE;
        }
        if($this->isHeader){
            $row = array_map($cleanKeys, $row);
            $this->isHeader = false;
            $this->header = $row;
            return $this->step();
        }

        // convert array to key/val array based on headers
        $results = array_combine($this->header, $row);

        // remap keys to standardized keys
        $results = $this->patchRow($results);
        $results = $this->remapKeys($results);


        return $results;
    }




}
