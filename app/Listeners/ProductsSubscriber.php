<?php

namespace App\Listeners;

use App\Events\ExampleEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductsSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        echo "in ProductsSubscriber __construct()\n";
    }

    /**
     * Handle user login events.
     */
    public function onDaySum($event) {

    }

    /**
     * Handle user login events.
     */
    public function onWeekSum($event) {}

    /**
     * Handle user login events.
     */
    public function onMonthSum($event) {}

    /**
     * Handle user login events.
     */
    public function onYearSum($event) {}

    /**
     * Register the listeners for the subscriber.
     *
     */
    public function subscribe($events)
    {
        echo "in ProductsSubscriber subscribe()\n";

        $events->listen(
            'App\Events\ExampleEvent',
            'App\Listeners\ProductsSubscriber@onDaySum'
        );

    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(ExampleEvent $event)
    {
        echo "in ProductsSubscriber handle()\n";
    }
}
