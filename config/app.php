<?php

return [
    'aliases' => [
        'Bugsnag' => Bugsnag\BugsnagLaravel\Facades\Bugsnag::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class

    ]

];
