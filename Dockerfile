FROM php:latest

# prep
RUN apt-get update && apt-get upgrade -y

# install required packages
RUN apt-get install git-core unzip wget -y
RUN docker-php-ext-install pdo_mysql
# install composer
COPY ./installcomposer.sh /
RUN /bin/bash /installcomposer.sh
RUN chmod a+x /usr/bin/composer

# setup project directory
RUN mkdir -p /app
COPY . /app
WORKDIR /app
RUN composer install
ENTRYPOINT ["php","artisan"]


#export DOCKER_HOST="tcp://192.168.1.40:2376"
#unset DOCKER_CERT_PATH
#unset DOCKER_TLS_VERIFY

#scp ~/Downloads/export7062_15_Nov_16_10_16_22.csv.zip goku@192.168.1.40:~/llexport.csv.zip
#ssh goku@192.168.1.40 "unzip -p ~/llexport.csv.zip > ~/llexport.csv && docker --host=localhost:2376 run -d --name=wall-e_import -v /home/goku/llexport.csv:/export.csv:ro wwtg/wall-e import"